export const serialize = (myObj) => {
    myObj = JSON.stringify(myObj, (key, value) => {
        if (typeof value === 'bigint') {
            return value.toString() + 'n'
        } else {
            return value;
        }
    });
    return myObj;
};