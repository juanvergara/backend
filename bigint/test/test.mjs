import { expect, assert } from 'chai';
import { serialize } from '../modules/serializer.mjs'
import { deserialize } from '../modules/deserializer.mjs'

const myObj = {
    testNumber: 123,
    testBigInt: 987n,
    nested: {
        myProp: 5n,
        myProp2: 10,
        myArray: [5n],
        myObject: {
            test: 5n
        }
    },
    myArray: [5, 50n]
}

const myObjSerialized = {
    testNumber: 123,
    testBigInt: '987n',
    nested: {
        myProp: '5n',
        myProp2: 10,
        myArray: ['5n'],
        myObject: {
            test: '5n'
        }
    },
    myArray: [5, '50n']
}

describe('BigInts Serializer and Deserializer', () => {
    it('Serialize is good', () => {
        expect(assert.deepEqual(serialize(myObj), JSON.stringify(myObjSerialized)));
    });
    it('Deserialize is good', () => {
        expect(assert.deepEqual(deserialize(serialize(myObj)), myObj));
    })
})