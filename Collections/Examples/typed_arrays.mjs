import { assert } from "chai";

// Typed array views work pretty much like normal arrays.
const f64a = new Float64Array(8);
console.log(`Begin ${f64a}`);
f64a[0] = 10;
f64a[1] = 20;
f64a[2] = f64a[0] + f64a[1];

console.log(f64a);
console.log(typeof f64a);