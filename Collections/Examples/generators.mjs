import { assert } from "chai";

function* genFunc1() {
    yield 'a';
    yield 'b';
}

const iterable = genFunc1();

assert.deepEqual([...iterable], ['a', 'b'])

//Can use for of loops too

for (const x of genFunc1()) {
    console.log(x);
}

//Yield pauses a generator function
let location = 0;
function* genFunc2() {
    location = 1; yield 'a';
    location = 2; yield 'b';
    location = 3;
}

//Creation of the iterator genFunc2()
const iter = genFunc2();

assert.equal(location, 0)

assert.deepEqual(
    iter.next(), { value: 'a', done: false });
// genFunc2() is now paused directly after the first `yield`:
assert.equal(location, 1);

assert.deepEqual(
    iter.next(), { value: 'b', done: false });
// genFunc2() is now paused directly after the second `yield`:
assert.equal(location, 2);


assert.deepEqual(
    iter.next(), { value: undefined, done: true });
// We have reached the end of genFunc2():
assert.equal(location, 3);