import { assert } from "chai";

function gfg() {
    var weakSetObject = new WeakSet();
    var objectOne = {};

    // add(value)
    weakSetObject.add(objectOne);
    console.log("objectOne added </br>");

    // has(value)
    console.log("WeakSet has objectOne : " +
        weakSetObject.has(objectOne));

    assert.equal(weakSetObject.has(objectOne), true)
}
gfg();

//Methods
// add(value): A new object is append with the given value
// delete(value): deletes the value from the weakset
// has(value): returns true if th value is present in the collection, false otherwise.
// length(): Returns the length of the weakSetObject