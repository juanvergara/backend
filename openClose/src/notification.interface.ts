export interface Notify {
    notify(platform: string): void;
}