import User from "./user";
import { Notify } from './notification.interface';

export default class NotificationCenter implements Notify{

    constructor(private user: User, private message: string) {
    }

    notify(platform: string): void {
        console.log(`Notify by email to ${this.user.name} message: ${this.message} in ${platform}`);
    }

    notifyByEmail(){
        console.log(`Notify by email to ${this.user.name} message: ${this.message}...`);
    }

    notifyBySms(){
        console.log(`Notify by SMS to ${this.user.name} message: ${this.message}...`);
    }

    // //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    // notifyByFacebook(){
    //     console.log(`Notify by Facebook to ${this.user.name} message: ${this.message}...`);
    // }

    // 100 diferentes tipos de notificacion (){
    //     console.log(`Notify by Facebook to ${this.user.name} message: ${this.message}...`);
    // }
}