import Book from './book';
import { Invoice } from './invoice';

export default class InvoiceSave extends Invoice{
  constructor(protected book: Book, protected quantity, protected discountRate, protected taxRate) {
    super(book, quantity, discountRate, taxRate);
  }
  
  saveToFile() {}
}