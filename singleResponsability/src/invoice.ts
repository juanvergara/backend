import Book from "./book";

export abstract class Invoice {
  constructor(
    protected book: Book,
    protected quantity: number,
    protected discountRate: number,
    protected taxRate: number
  ) { }

  calculateTotal(): void { }
  printInvoice(): void { }
  saveToFile(): void { }
}