import Book from './book';
import { Invoice } from "./invoice";

export class InvoiceTotal extends Invoice {
  private total: number;

  constructor(protected book: Book, protected quantity, protected discountRate, protected taxRate) {
    super(book, quantity, discountRate, taxRate);
  }
  calculateTotal() {
    console.log(`${this.book.getName()} Invoice quantity: ${this.quantity}`);
    console.log(`Discount Rate: ${this.discountRate}`);
    console.log(`Tax Rate: ${this.taxRate}`);
    console.log(`TOTAL: ${this.total}`);
  }
}
