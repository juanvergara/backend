import Book from "./book";
import InvoiceSave from "./invoice_save";

const book = new Book("Clean Code", "Bob", 1995, 49, "SD-456-ASD");
// const invoice = new Invoice(book, 1, 50, 0.14);
// invoice.calculateTotal();

const persistence = new InvoiceSave(book, 100, 20, 11);
persistence.saveToFile();