import { Board } from './board';
import { Piece } from './piece';
import { Spot } from './spot';


export class King extends Piece {
    private castlingDone = false;

    constructor(pieceColor: boolean) {
        super(pieceColor);
    }

    isCastlingDone() {
        return this.castlingDone;
    }

    setCastlingDone(castlingDone: boolean) {
        this.castlingDone = castlingDone;
    }

    canMove(board: Board, startSpot: Spot, endSpot: Spot) {
        const spotsCanMove = 1;
        if (endSpot.getPiece().isWhite() === this.isWhite()) {
            return false;
        }

        let rowSpot = Math.abs(startSpot.getRowPosition() - endSpot.getRowPosition());
        let columnSpot = Math.abs(startSpot.getColumnPosition() - endSpot.getColumnPosition());
        if (rowSpot + columnSpot === spotsCanMove) {
            return true
        } //Identify what is this. 

        return this.isValidCastling(board, startSpot, endSpot);
    }

    isValidCastling(board: Board, startSpot: Spot, endSpot: Spot) {
        if (this.isCastlingDone()) {
            return false;
        }
    }


}