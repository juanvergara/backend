export class List<T>{
    private items: Array<T>;
    
    constructor(){
        this.items = [];
    }

    size() {
        return this.items.length;
    }

    add(value: T) {
       return this.items.push(value);
    }

    get(index: number): T {
        return this.items[index];
    }

}