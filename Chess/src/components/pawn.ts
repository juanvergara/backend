import { Board } from './board';
import { Piece } from './piece';
import { Spot } from './spot';

export class Pawn extends Piece {
    
    constructor(pieceColor: boolean) {
        super(pieceColor);
    }
    
    canMove(board: Board, startSpot: Spot, endSpot: Spot) {
        throw new Error('Method not implemented.');
    }

}