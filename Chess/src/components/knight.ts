import { Board } from './board';
import { Piece } from './piece';
import { Spot } from './spot';

export class Knight extends Piece {
    
    constructor(pieceColor: boolean) {
        super(pieceColor);
    }
    
    canMove(board: Board, startSpot: Spot, endSpot: Spot) {
        if(endSpot.getPiece().isWhite() === this.isWhite()){
            return false;
        }

        let rowSpot = Math.abs(startSpot.getRowPosition() - endSpot.getRowPosition());
        let columnSpot = Math.abs(startSpot.getColumnPosition() - endSpot.getColumnPosition());

        return rowSpot * columnSpot === 2;
    }

}