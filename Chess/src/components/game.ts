import { Player } from './player';
import { Board } from './board';
import { GameStatus } from './gameStatus';
import { Move } from './move';
import { List } from './list';

export class Game {
    private players: Player[];
    private board: Board;
    private currentTurn: Player;
    private status: GameStatus;
    private movesPlayed: List<Move>;

    initialize(playerOne: Player, playerTwo: Player) {
        this.players[0] = playerOne;
        this.players[1] = playerTwo;

        this.board.resetBoard();

        if (playerOne.isWhiteSide()) {
            this.currentTurn = playerOne;
        } else {
            this.currentTurn = playerTwo;
        }

        this.movesPlayed;
    }

    isEnd(){
        return this.getStatus() !== GameStatus.ACTIVE;
    }

    getStatus(){
        return this.status;
    }

    setStatus(status: GameStatus) {
        this.status = status;
    }

    playerMove(player: Player, startRow: number, startColumn: number, endRow: number, endColumn: number) {
        let startSpot = this.board.getSpots(startRow, endRow);
        let endSpot = this.board.getSpots(startColumn, endColumn);
        let move = new Move(player, startSpot, endSpot);
        return this.makeMove(move, player);
    }

    makeMove(move: Move, player: Player) {
        let sourcePiece = move.getStart().getPiece();
        if(sourcePiece === null){
            return false;
        }
        if(player !== this.currentTurn){
            return false;
        }
        if(sourcePiece.isWhite() !== player.isWhiteSide()){
            return false;
        }

        if(!sourcePiece.canMove(this.board, move.getStart(), move.getEnd())){
            return false;
        }

        if(sourcePiece !== null){
            sourcePiece.setKilled(true);
            move
        }
    }
}