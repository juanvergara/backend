import { Board } from "./board";
import { Spot } from './spot';

export abstract class Piece {
    private killed = false;
    private pieceColor = false;

    constructor(pieceColor: boolean) {
        this.pieceColor = pieceColor;
    }

    isWhite() {
        return this.pieceColor;
    }

    setWhite(pieceColor: boolean) {
        this.pieceColor = pieceColor;
    }

    isKilled() {
        return this.killed;
    }

    setKilled(killed: boolean) {
        this.killed = killed;
    }

    abstract canMove(board: Board, startSpot: Spot, endSpot: Spot);
}