export abstract class Player {
    whiteSide: boolean;
    humanPlayer: boolean;

    isWhiteSide(){
        return this.whiteSide;
    }

    isHumanPlayer(){
        return this.humanPlayer;
    }
}

export class HumanPlayer extends Player {
    constructor(whiteSide: boolean){
        super();
        this.whiteSide = whiteSide;
        this.humanPlayer = true;
    }
}

export class ComputerPlayer extends Player {
    constructor(whiteSide: boolean){
        super();
        this.whiteSide = whiteSide;
        this.humanPlayer = false;
    }
}