import { Bishop } from './bishop';
import { Knight } from './knight';
import { Queen } from './queen';
import { Rook } from './rook';
import { Spot } from './spot';
import { King } from './king';
import { Pawn } from './pawn';

export class Board {
    public spots: Spot[][];

    getSpots(initial: number, final: number) {
        if (initial < 0 || initial > 7 || final < 0 || final > 7) {
            throw Error("Index out of bound");
        }
        return this.spots[initial][final];
    }

    resetBoard() {
        //Initialize white pieces
        this.spots[0][0] = new Spot(0, 0, new Rook(true));
        this.spots[0][1] = new Spot(0, 1, new Knight(true));
        this.spots[0][2] = new Spot(0, 2, new Bishop(true));
        this.spots[0][3] = new Spot(0, 3, new Queen(true));
        this.spots[0][4] = new Spot(0, 3, new King(true));
        this.spots[0][5] = new Spot(0, 5, new Bishop(true));
        this.spots[0][6] = new Spot(0, 6, new Knight(true));
        this.spots[0][7] = new Spot(0, 7, new Rook(true));
        this.spots[1][0] = new Spot(1, 1, new Pawn(true));
        this.spots[1][1] = new Spot(1, 1, new Pawn(true));
        this.spots[1][2] = new Spot(1, 1, new Pawn(true));
        this.spots[1][3] = new Spot(1, 1, new Pawn(true));
        this.spots[1][4] = new Spot(1, 1, new Pawn(true));
        this.spots[1][5] = new Spot(1, 1, new Pawn(true));
        this.spots[1][6] = new Spot(1, 1, new Pawn(true));
        this.spots[1][7] = new Spot(1, 1, new Pawn(true));

        //Initialize black pieces
        this.spots[7][0] = new Spot(7, 0, new Rook(true));
        this.spots[7][1] = new Spot(7, 1, new Knight(true));
        this.spots[7][2] = new Spot(7, 2, new Bishop(true));
        this.spots[7][3] = new Spot(7, 3, new Queen(true));
        this.spots[7][4] = new Spot(7, 3, new King(true));
        this.spots[7][5] = new Spot(7, 5, new Bishop(true));
        this.spots[7][6] = new Spot(7, 6, new Knight(true));
        this.spots[7][7] = new Spot(7, 7, new Rook(true));
        this.spots[6][0] = new Spot(6, 1, new Pawn(true));
        this.spots[6][1] = new Spot(6, 1, new Pawn(true));
        this.spots[6][2] = new Spot(6, 1, new Pawn(true));
        this.spots[6][3] = new Spot(6, 1, new Pawn(true));
        this.spots[6][4] = new Spot(6, 1, new Pawn(true));
        this.spots[6][5] = new Spot(6, 1, new Pawn(true));
        this.spots[6][6] = new Spot(6, 1, new Pawn(true));
        this.spots[6][7] = new Spot(6, 1, new Pawn(true));

        for( let i = 2; i < 6; i++){
            for( let j = 0; j<8; j++){
                this.spots[i][j] = new Spot(i, j, null)
            }
        }
    }
}