import { Piece } from "./piece";

export class Spot {
    private piece: Piece;
    private rowPosition: number;
    private columnPosition: number;

    constructor(rowPosition: number, columnPosition: number, piece: Piece) {
        this.piece = piece;
        this.columnPosition = columnPosition;
        this.rowPosition = rowPosition;
    }

    getPiece() {
        return this.piece;
    }

    getRowPosition() {
        return this.rowPosition;
    }

    getColumnPosition() {
        return this.columnPosition;
    }
}