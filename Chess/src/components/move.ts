import { Player } from './player';
import { Spot } from './spot';
import { Piece } from './piece';

export class Move {
    private player: Player;
    protected startSpot: Spot;
    protected endSpot: Spot;
    private priceMoved: Piece;
    private pieceKilled: Piece;
    private castlingMove = false;

    constructor(player: Player, startSpot: Spot, endSpot: Spot) {
        this.player = player;
        this.startSpot = startSpot;
        this.endSpot = endSpot;
        this.priceMoved = startSpot.getPiece();
    }

    isCastlingMove() {
        return this.castlingMove;
    }

    setCastlingMove(castlingMove: boolean) {
        this.castlingMove = castlingMove;
    }

    getStart() {
        return this.startSpot;
    }

    getEnd(){
        return this.endSpot;
    }
}