export interface RepositoryUser<T> {

    insert(entity: T);

    update(entity: T);

    get(): T;

    delete(id: string): void;
}

export interface RepositoryLog<T> {

    insert(entity: T);

    // update(entity: T);

    get(): T;

    // delete(id: string): void;
}