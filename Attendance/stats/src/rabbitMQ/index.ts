import amqp from 'amqplib';

const queueName = "Attendance";

export async function receiveMsg() {
    const connection = await amqp.connect('amqp://localhost');
    const channel = await connection.createChannel();
    await channel.assertQueue(queueName, { durable: false });
    console.log(`Waiting for messages in queue: ${queueName}`);
    channel.consume(queueName, msg => {
        console.log("[X] Received:", msg?.content);
    }, { noAck: true })
}