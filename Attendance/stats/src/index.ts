import * as config from './config/index';

import express from 'express';
import { receiveMsg } from './rabbitMQ';

const app = express();

const main = async () => {
    try {
        app.use(express.json());

        app.listen(config.default.PORT, () => {
            console.log(`Running on port ${config.default.PORT}`);

        })

        await receiveMsg();
    }
    catch (error) {
        console.error(error);
        throw new Error('ERROR');
    }
};

main();