"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAttendances = exports.createAttendances = exports.getAttendances = void 0;
const index_1 = require("../repository/index");
const index_2 = require("../rabbitMQ/index");
const getAttendances = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const attendance = yield (0, index_1.getAttendance)();
        if (attendance) {
            console.log(attendance);
            yield (0, index_2.sendMsg)(attendance);
            return res.status(200).json({ success: true, message: "List of all Attendance", data: attendance });
        }
        else {
            return res.status(404).json({ success: false, message: "Not Found", data: [] });
        }
    }
    catch (error) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] });
    }
});
exports.getAttendances = getAttendances;
const createAttendances = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const results = yield (0, index_1.createAttendance)(req.body);
        return res.status(201).json({ success: true, message: "The attendance was created correctly", data: results });
    }
    catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] });
    }
});
exports.createAttendances = createAttendances;
const deleteAttendances = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const userId = req.params.idUser;
    try {
        const attendance = yield (0, index_1.getAttendanceById)(req.params.idAttendance);
        if ((attendance === null || attendance === void 0 ? void 0 : attendance.userId) === userId) {
            yield (0, index_1.deleteAttendance)(req.params.idAttendance);
            return res.json({ success: true, message: "Attendance Delete correctly", data: attendance });
        }
        else if ((attendance === null || attendance === void 0 ? void 0 : attendance.userId) !== userId) {
            return res.status(401).json({ success: false, message: "User Id not match with user id in the attendance", data: [] });
        }
        return res.status(404).json({ success: false, message: 'User Not Found', data: [] });
    }
    catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] });
    }
});
exports.deleteAttendances = deleteAttendances;
