"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const attendance_controller_1 = require("../controllers/attendance.controller");
const router = (0, express_1.Router)();
router.get('/api/attendance', attendance_controller_1.getAttendances);
router.post('/api/attendance', attendance_controller_1.createAttendances);
router.delete('/api/attendance/:idAttendance/:idUser', attendance_controller_1.deleteAttendances);
exports.default = router;
