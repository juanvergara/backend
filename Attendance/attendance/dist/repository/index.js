"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAttendance = exports.createAttendance = exports.getAttendanceById = exports.getAttendance = void 0;
const typeorm_1 = require("typeorm");
const Attendance_1 = require("../entities/Attendance");
function getAttendance() {
    return __awaiter(this, void 0, void 0, function* () {
        const results = yield (0, typeorm_1.getRepository)(Attendance_1.Attendance).find();
        return results;
    });
}
exports.getAttendance = getAttendance;
function getAttendanceById(id) {
    return __awaiter(this, void 0, void 0, function* () {
        const results = yield (0, typeorm_1.getRepository)(Attendance_1.Attendance).findOne(id);
        return results;
    });
}
exports.getAttendanceById = getAttendanceById;
function createAttendance(attendance) {
    return __awaiter(this, void 0, void 0, function* () {
        const newAttendance = (0, typeorm_1.getRepository)(Attendance_1.Attendance).create(attendance);
        const results = yield (0, typeorm_1.getRepository)(Attendance_1.Attendance).save(newAttendance);
        return results;
    });
}
exports.createAttendance = createAttendance;
function deleteAttendance(id) {
    return __awaiter(this, void 0, void 0, function* () {
        const results = yield (0, typeorm_1.getRepository)(Attendance_1.Attendance).delete(id);
        return results;
    });
}
exports.deleteAttendance = deleteAttendance;
