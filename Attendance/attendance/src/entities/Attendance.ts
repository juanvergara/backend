import { Entity, BaseEntity, Column, ObjectIdColumn, ObjectID} from 'typeorm';


@Entity('attendance')
export class Attendance extends BaseEntity {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column({
        type: 'date',
    })
    horaInicio: Date;

    @Column({
        type: 'date'
    })
    horaFin: Date;

    @Column({
        type: 'date'
    })
    fecha: Date;

    @Column({
        type: "array"
    })
    notas: Object;

    @Column({
        type: 'uuid'
    })
    userId: string;
}