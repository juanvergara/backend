import { Request, Response } from 'express';
import { getAttendance, createAttendance, getAttendanceById, deleteAttendance } from "../repository/index";
import { sendMsg } from "../rabbitMQ/index";


export const getAttendances = async (req: Request, res: Response): Promise<Response> => {
    try {
        const attendance = await getAttendance();
        if (attendance) {
            console.log(attendance);            
            await sendMsg(attendance);
            return res.status(200).json({ success: true, message: "List of all Attendance", data: attendance });
        } else {
            return res.status(404).json({ success: false, message: "Not Found", data: [] })
        }
    } catch (error) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] })
    }
}


export const createAttendances = async (req: Request, res: Response): Promise<Response> => {
    try {
        const results = await createAttendance(req.body);

        return res.status(201).json({ success: true, message: "The attendance was created correctly", data: results });
    } catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] })
    }
}


export const deleteAttendances = async (req: Request, res: Response): Promise<Response> => {
    const userId = req.params.idUser;
    try {
        const attendance = await getAttendanceById(req.params.idAttendance);        

        if (attendance?.userId === userId) {
            await deleteAttendance(req.params.idAttendance);
            return res.json({ success: true, message: "Attendance Delete correctly", data: attendance });
        }
        else if (attendance?.userId !== userId) {
            return res.status(401).json({ success: false, message: "User Id not match with user id in the attendance", data: [] });
        }

        return res.status(404).json({ success: false, message: 'User Not Found', data: [] });

    } catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] })
    }
}


