import amqp from 'amqplib';

const queueName = "Attendance";

export async function sendMsg(msg: any) {
    const connection = await amqp.connect('amqp://localhost');
    const channel = await connection.createChannel();
    await channel.assertQueue(queueName, { durable: false });
    channel.sendToQueue(queueName, Buffer.from(msg));
}