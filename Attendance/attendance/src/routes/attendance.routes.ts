import { Router } from 'express';
import { getAttendances, createAttendances, deleteAttendances } from '../controllers/attendance.controller';
const router = Router();

router.get('/api/attendance', getAttendances);
router.post('/api/attendance', createAttendances);
router.delete('/api/attendance/:idAttendance/:idUser', deleteAttendances);


export default router;