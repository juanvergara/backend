import { getRepository } from "typeorm";
import { Attendance } from '../entities/Attendance';

export async function getAttendance() {
    const results = await getRepository(Attendance).find();
    return results;
}

export async function getAttendanceById(id: any) {
    const results = await getRepository(Attendance).findOne(id);    
    return results;
}

export async function createAttendance(attendance: Object) {
    const newAttendance = getRepository(Attendance).create(attendance);
    const results = await getRepository(Attendance).save(newAttendance);

    return results;
}

export async function deleteAttendance(id: any) {
    const results = await getRepository(Attendance).delete(id);

    return results;
}

