"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.createUsers = exports.Users = void 0;
const index_1 = require("../services/index");
const index_2 = require("../repository/index");
const Users = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        if (!req.query.realName && !req.query.nickName) {
            const users = yield (0, index_2.getUsers)();
            const allAttendance = yield (0, index_1.getAttendance)();
            return res.status(200).json({ success: true, message: "List of all users", data: users, attendance: allAttendance });
        }
        else if (req.query.realName || req.query.nickName) {
            const users = yield (0, index_2.getUsersByNickNameOrRealName)(req.query.nickName, req.query.realName);
            return res.status(200).json({ success: true, message: "List of users search by realName and/or nickName", data: users });
        }
        else {
            return res.status(404).json({ success: false, message: "Not Found", data: [] });
        }
    }
    catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] });
    }
});
exports.Users = Users;
const createUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const results = yield (0, index_2.createUser)(req.body);
        return res.status(201).json({ success: true, message: "User created correctly", data: results });
    }
    catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] });
    }
});
exports.createUsers = createUsers;
const deleteUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield (0, index_2.getUserById)(req.params.id);
        if (user) {
            (0, index_2.deleteUserById)(req.params.id);
            return res.json({ success: true, message: "User Delete correctly", data: user });
        }
        return res.status(404).json({ success: false, message: 'User Not Found', data: [] });
    }
    catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] });
    }
});
exports.deleteUser = deleteUser;
