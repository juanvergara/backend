"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUserById = exports.createUser = exports.getUsersByNickNameOrRealName = exports.getUserById = exports.getUsers = void 0;
const typeorm_1 = require("typeorm");
const User_1 = require("../entities/User");
function getUsers() {
    return __awaiter(this, void 0, void 0, function* () {
        const results = yield (0, typeorm_1.getRepository)(User_1.User).find();
        return results;
    });
}
exports.getUsers = getUsers;
function getUserById(id) {
    return __awaiter(this, void 0, void 0, function* () {
        const results = yield (0, typeorm_1.getRepository)(User_1.User).findOne(id);
        return results;
    });
}
exports.getUserById = getUserById;
function getUsersByNickNameOrRealName(nickName, realName) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log(nickName, realName);
        const results = yield (0, typeorm_1.getRepository)(User_1.User)
            .createQueryBuilder('user')
            .where("user.realName LIKE :realName OR user.nickName LIKE :nickName", {
            realName: `%${realName}%`, nickName: `%${nickName}%`
        })
            .getMany();
        console.log(results);
        return results;
    });
}
exports.getUsersByNickNameOrRealName = getUsersByNickNameOrRealName;
function createUser(user) {
    return __awaiter(this, void 0, void 0, function* () {
        const newUser = (0, typeorm_1.getRepository)(User_1.User).create(user);
        const results = yield (0, typeorm_1.getRepository)(User_1.User).save(newUser);
        return results;
    });
}
exports.createUser = createUser;
function deleteUserById(id) {
    return __awaiter(this, void 0, void 0, function* () {
        const results = yield (0, typeorm_1.getRepository)(User_1.User).delete(id);
        return results;
    });
}
exports.deleteUserById = deleteUserById;
