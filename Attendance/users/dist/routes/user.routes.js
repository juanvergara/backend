"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_controller_1 = require("../controllers/user.controller");
const router = (0, express_1.Router)();
router.get('/api/users', user_controller_1.Users);
router.post('/api/users', user_controller_1.createUsers);
router.delete('/api/users/:id', user_controller_1.deleteUser);
exports.default = router;
