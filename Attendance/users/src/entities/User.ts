import { Entity, BaseEntity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('user')
export class User extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: number;

    @Column()
    nickName: string;

    @Column()
    realName: string;

    @Column({
        default: 0
    })
    attendance: number;
}