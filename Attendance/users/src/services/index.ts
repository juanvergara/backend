import axios from 'axios';

export async function getAttendance() {
    const results = await axios.get('http://localhost:3500/api/attendance').then((response) => {
        return response.data;
    }).catch((error) => {
        console.log("error", error);
        throw Error(error);
    });
    return results;
}
