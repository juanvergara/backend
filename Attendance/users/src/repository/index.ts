import { getRepository } from "typeorm";
import { User } from '../entities/User';

export async function getUsers() {
    const results = await getRepository(User).find();
    return results;
}

export async function getUserById(id: any) {
    const results = await getRepository(User).findOne(id);
    return results;
}

export async function getUsersByNickNameOrRealName(nickName: any, realName: any) {
    console.log(nickName, realName);
    
    const results = await getRepository(User)
        .createQueryBuilder('user')
        .where("user.realName LIKE :realName OR user.nickName LIKE :nickName", {
            realName: `%${realName}%`, nickName: `%${nickName}%`
        })
        .getMany();
    console.log(results);

    return results;
}

export async function createUser(user: Object) {
    const newUser = getRepository(User).create(user);
    const results = await getRepository(User).save(newUser);
    return results;
}

export async function deleteUserById(id: string) {
    const results = await getRepository(User).delete(id);
    return results;
}