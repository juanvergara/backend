import 'reflect-metadata'
import * as config from './config/index';

import { createConnection } from 'typeorm';
import express from 'express';
import userRoutes from './routes/user.routes';

const app = express();

const main = async () => {
    try {
        console.log('Connected to MySQL');
        app.use(express.json());
        app.use(userRoutes);
        await createConnection();

        app.listen(config.default.PORT, () => {
            console.log(`Running on port ${config.default.PORT}`);

        })
    }
    catch (error) {
        console.error(error);
        throw new Error('ERROR');
    }
};

main();