import { Request, Response } from 'express';
import { getAttendance } from "../services/index";
import { getUsers, getUsersByNickNameOrRealName, createUser, getUserById, deleteUserById } from "../repository/index";


export const Users = async (req: Request, res: Response): Promise<Response> => {
    try {
        if (!req.query.realName && !req.query.nickName) {
            const users = await getUsers();
            const allAttendance = await getAttendance();
            
            return res.status(200).json({ success: true, message: "List of all users", data: users, attendance: allAttendance });
        } else if (req.query.realName || req.query.nickName) {
            const users = await getUsersByNickNameOrRealName(req.query.nickName, req.query.realName);

            return res.status(200).json({ success: true, message: "List of users search by realName and/or nickName", data: users });
        } else {
            return res.status(404).json({ success: false, message: "Not Found", data: [] });
        }
    } catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] });
    }
}


export const createUsers = async (req: Request, res: Response): Promise<Response> => {
    try {
        const results = await createUser(req.body);

        return res.status(201).json({ success: true, message: "User created correctly", data: results });
    } catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] });
    }
}

export const deleteUser = async (req: Request, res: Response): Promise<Response> => {
    try {
        const user = await getUserById(req.params.id)
        if (user) {
            deleteUserById(req.params.id);
            return res.json({ success: true, message: "User Delete correctly", data: user });
        }

        return res.status(404).json({ success: false, message: 'User Not Found', data: [] });

    } catch (err) {
        return res.status(500).json({ success: false, message: "Server Error", data: [] });
    }
}