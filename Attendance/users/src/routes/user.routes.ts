import { Router } from 'express';
import { Users, createUsers, deleteUser } from '../controllers/user.controller';
const router = Router();

router.get('/api/users', Users);
router.post('/api/users', createUsers);
router.delete('/api/users/:id', deleteUser);

export default router;