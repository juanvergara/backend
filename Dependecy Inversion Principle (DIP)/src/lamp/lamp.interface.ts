export interface ILamp {
    turnOn(): void;
    turnOff(): void;
    getStatus(): boolean;
}