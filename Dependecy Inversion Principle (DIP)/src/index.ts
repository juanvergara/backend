import Lamp from "./lamp/lamp";
import Button from './button/button';

const lamp = new Lamp();
const button = new Button(lamp);

button.onButtonListener();
