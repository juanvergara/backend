import { ILamp } from "../lamp/lamp.interface";

export default class Button {
    constructor(protected lamp: ILamp) {
    }
    onButtonListener() {
        if (this.lamp.getStatus()) {
            this.lamp.turnOff()
        } else {
            this.lamp.turnOn();
        }
    }
}