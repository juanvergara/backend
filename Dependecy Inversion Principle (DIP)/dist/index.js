"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lamp_1 = __importDefault(require("./lamp/lamp"));
const button_1 = __importDefault(require("./button/button"));
const lamp = new lamp_1.default();
const button = new button_1.default(lamp);
button.onButtonListener();
//# sourceMappingURL=index.js.map