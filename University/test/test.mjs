import { assert, expect } from "chai";
import { getAvailableDocs } from '../University.mjs'
import { olivosDoctors } from '../modules/olivosDocs.mjs'
import { SanPedroDoctors } from '../modules/sanPedroDocs.mjs'

let time = new Date()
let hours = time.getHours();

describe('Test Doctors Availability', () => {
    if (hours >= 19 || hours < 5) {
        it("Get San Pedro's Doctors", () => {
            expect(assert.deepEqual(getAvailableDocs(hours), SanPedroDoctors))
        })
    } else {
        it("Get Olivos' Doctors", () => {
            expect(assert.deepEqual(getAvailableDocs(hours), olivosDoctors))
        })
    }
})