export function getAvailableDocs(time) {
    if (time >= 19 || time < 5) {
        return import('./modules/sanPedroDocs.mjs').then(value => {
            return value.SanPedroDoctors
        })
    } else {
        return import('./modules/olivosDocs.mjs').then(value => {
            return value.olivosDoctors
        })
    }
}

let time = new Date()
let hours = time.getHours();

console.log(getAvailableDocs(hours))